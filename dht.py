#!/usr/bin/env python3

import argparse
import json
import os
import signal
import sys
import time

import adafruit_dht
import board
import paho.mqtt.client as mqtt

try:
    gpio = getattr(board, os.getenv('GPIO', 'D27'))
    sensor = adafruit_dht.DHT22(gpio)
except AttributeError as error:
    print(error)
    exit()

parser = argparse.ArgumentParser(description='DHT22 to MQTT exporter')
parser.add_argument('-H', '--mqtt-broker', help='mqtt broker address', type=str,
                    default=os.getenv('MQTT_BROKER', 'localhost'))
parser.add_argument('-p', '--mqtt-port', help='mqtt broker port', type=int, default=os.getenv('MQTT_PORT', 1883))
parser.add_argument('-t', '--topic', help='mqtt topic prefix', type=str, default=os.environ.get('MQTT_TOPIC', 'dht'))
parser.add_argument('-i', '--interval', help='report interval', type=int, default=os.getenv('INTERVAL', 60))
args = parser.parse_args()

client = mqtt.Client()
client.connect(args.mqtt_broker, args.mqtt_port, 60)
client.loop_start()


def signal_handler(sig, frame):
    sensor.exit()
    client.disconnect()
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)

while True:
    try:
        temperature_c = sensor.temperature
        humidity = sensor.humidity

        print("{:.1f} C    Humidity: {}% ".format(temperature_c, humidity))

        payload = {"temperature": temperature_c, "humidity": humidity}
        client.publish(args.topic, json.dumps(payload))

    except RuntimeError as error:
        print(error.args[0])
        time.sleep(2.0)
        continue
    except Exception as error:
        sensor.exit()
        raise error

    time.sleep(args.interval)
